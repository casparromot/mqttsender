#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>

namespace Ui
{
    class MainWindow;
}

namespace MQTTSender
{
    class MainWindow : public QMainWindow
    {
        Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private:
        void
        load();
        void
        save();
        void
        addRow(const QString& file = "", const QString& topic = "");
        void
        setMosquittoPath(const QString& path = "");
        void
        contextMenuEvent(QContextMenuEvent * event) override;

        Ui::MainWindow *_ui;
        QTableWidget *_table;
    };
}
#endif