#-------------------------------------------------
#
# Project created by QtCreator 2017-07-17T18:42:34
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = MQTTSender
TEMPLATE = app

INCLUDEPATH += $$PWD/inc

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        src/Main.cpp\
        src/MainWindow.cpp

HEADERS  += \
        inc/MainWindow.hpp

FORMS    += \
        src/MainWindow.ui
