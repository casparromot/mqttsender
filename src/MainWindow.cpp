#include "MainWindow.hpp"
#include "ui_mainwindow.h"

#include <QPushButton>
#include <QFileDialog>
#include <QProcess>
#include <QDesktopWidget>
#include <QMenu>
#include <QHeaderView>
#include <QContextMenuEvent>

namespace MQTTSender
{
    const QString DEFAULT_MOSQUITTO_PATH = "C:/Program Files (x86)/mosquitto";
    const QString MOSQUITTO_PUB_FILE_NAME = "mosquitto_pub.exe";

    enum Columns { Browse, Message, Topic, Send };

    MainWindow::MainWindow(QWidget *parent)
        : QMainWindow(parent)
        , _ui(new Ui::MainWindow)
    {
        _ui->setupUi(this);
        QSize size = QDesktopWidget().availableGeometry(QDesktopWidget().primaryScreen()).size();
        size.scale(size.width() * 0.5, size.height() * 0.5, Qt::KeepAspectRatio);
        setWindowTitle("MQTT Sender");
        setMosquittoPath(DEFAULT_MOSQUITTO_PATH);
        _ui->LastMessage->setStyleSheet("QLabel { color : green; }");
        _table = _ui->Table;
        _table->setColumnCount(4);
        _table->setHorizontalHeaderLabels({ "Browse", "Message", "Topic", "Send" });
        _table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        _table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
        _table->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Fixed);
        load();
        resize(size);
        connect(_ui->AddButton, &QPushButton::clicked, [=] {addRow(); });
        connect(_ui->Settings, &QPushButton::clicked, [=] {setMosquittoPath(); });
    }

    MainWindow::~MainWindow()
    {
        save();
        delete _ui;
    }

    void
    MainWindow::load()
    {
        QFile file("save.txt");
        file.open(QIODevice::ReadOnly);
        QString fileContents = QString(file.readAll());
        file.close();
        auto lines = fileContents.split("\n", QString::SkipEmptyParts);
        if(!lines.count())
            return;
        if(lines.first().split(",").first() == "mosquitto")
            setMosquittoPath(lines.takeFirst().split(",").last());
        for(auto line : lines)
            addRow(line.split(",").first(), line.split(",").last());
    }

    void
    MainWindow::save()
    {
        QString saveString;
        saveString += ("mosquitto," + _ui->Path->text().trimmed() + "\n");
        for(int i = 0; i < _table->rowCount(); i++)
        {
            auto message = _table->item(i, Message)->data(Qt::DisplayRole).toString().trimmed();
            auto topic = _table->item(i, Topic)->data(Qt::DisplayRole).toString().trimmed();
            if(!message.isEmpty() && !topic.isEmpty())
                saveString += (message + "," + topic + "\n");
        }
        QFile file("save.txt");
        file.open(QIODevice::WriteOnly);
        file.write(saveString.toStdString().c_str());
        file.close();
    }

    void
    MainWindow::addRow(const QString &file, const QString &topic)
    {
        int row = _table->rowCount();
        _table->insertRow(row);
        _table->setItem(row, Message, new QTableWidgetItem(file));
        _table->setItem(row, Topic, new QTableWidgetItem(topic));
        auto browseButton = new QPushButton("Browse");
        _table->setIndexWidget(_table->model()->index(row, Browse), browseButton);
        connect(browseButton, &QPushButton::clicked, [=]()
        {
            _table->setItem(row, Message, new QTableWidgetItem(QFileDialog::getOpenFileName()));
        });
        auto sendButton = new QPushButton("Send");
        _table->setIndexWidget(_table->model()->index(row, Send), sendButton);
        connect(sendButton, &QPushButton::clicked, [=]()
        {
            QString message = _table->item(row, Message)->data(Qt::DisplayRole).toString();
            QFile file(message);
            if(file.exists())
            {
                file.open(QIODevice::ReadOnly);
                message = QString(file.readAll());
                file.close();
                message.replace("\n", "").simplified();
                message.replace("\t", " ").simplified();
                message.replace("  ", " ").simplified();
            }
            QString command = QDir(_ui->Path->text()).filePath(MOSQUITTO_PUB_FILE_NAME);
            QStringList arguments = { "-t", topic, "-m", message };
            bool success = QProcess::startDetached(command, arguments);
            if(success)
                _ui->LastMessage->setText(message);
        });
    }

    void
    MainWindow::setMosquittoPath(const QString &path)
    {
        QString temp = path;
        if(temp.isEmpty())
            temp = QFileDialog::getExistingDirectory(this);
        if(!QFile(QDir(temp).filePath(MOSQUITTO_PUB_FILE_NAME)).exists())
            _ui->Path->setStyleSheet("QLabel { color : red; }");
        else
            _ui->Path->setStyleSheet("QLabel { color : black; }");
        _ui->Path->setText(temp);
    }

    void
    MainWindow::contextMenuEvent(QContextMenuEvent *event)
    {
        if(!_table->itemAt(_table->viewport()->mapFromGlobal(event->globalPos())))
            return;
        QMenu menu(this);
        QAction *u = menu.addAction(tr("remove"));
        connect(u, &QAction::triggered, [=] {_table->removeRow(_table->itemAt(_table->viewport()->mapFromGlobal(event->globalPos()))->row()); });
        menu.exec(event->globalPos());
    }
}