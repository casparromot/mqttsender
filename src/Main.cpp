#include "MainWindow.hpp"
#include <QApplication>

int
main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MQTTSender::MainWindow w;
    w.show();

    return a.exec();
}