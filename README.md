# README #

MQTTSender

### What is this repository for? ###

This application can be used to conveniently send MQTT messages.
Steps to send a message:

1. Add a row
2. Specify the message. The message field can hold the message itself, or a path to an existing file, which will be read by the application
3. Specify the topic, to send the message to
4. Click "Send"

Upon closing the application, the rows, you added, will be saved and they will be restored upon reopening the application.

NOTE: inline multiline messages are not currently supported and will mess up the saving/loading process. If you want to 
send a multiline message, put it in a file and in the "Message" field specify the absolute file path.

### How do I get set up? ###

Dependencies:
-Qt5
-Mosquitto must be installed in the system

1. Clone the reposiory
2. Run CMake and set it up
3. Build application
4. Run the application and specify the root directory of mosquitto (Defaulted to C:\Program Files (x86)\mosquitto)

### Who do I talk to? ###
Repository admin: Caspar Romot